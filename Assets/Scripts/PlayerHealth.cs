﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public float totalHealth;
    public float currHealth;

    // Use this for initialization
    void Start()
    {
     currHealth = totalHealth;
    }


    // Update is called once per frame
    void Update()
    {
     if(Input.GetMouseButtonDown(0))
      {
       if (currHealth != 0)
        {
         takeDamage();
        }
       Limit();
      }
    }

    
    void takeDamage()
    {   
     currHealth -= 5; 
     transform.localScale = new Vector3((currHealth / totalHealth), 1, 1);
    }


    void Limit()
     {       
      if (currHealth <= 0)
       {
        currHealth = 0;
        SceneManager.LoadScene("Menu");
       }       
     }

}
